// https://eslint.org/docs/user-guide/configuring

module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module'
  },
  env: {
    browser: true,
  },
  // https://github.com/standard/standard/blob/master/docs/RULES-en.md
  extends: 'standard',
  // required to lint *.vue files
  plugins: [
    'html'
  ],
  globals: {
    'AMap': false,
    'AMapUI': false,
    'JMessage': false
  },
  // add your custom rules here
  rules: {
    // allow async-await
    'generator-star-spacing': 'off',
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    // 不检测文件末尾是否为空行
    'eol-last': 0,
    // 不检测function的左括号{ 前的空格
    'space-before-function-paren': 0,
    // 禁用 promise的reject方法必须以Error对象返回
    'prefer-promise-reject-errors': 'off',
    // 禁用每个 var 关键字单独声明一个变量
    'one-var': 0,
    // 禁用 要求或禁止在 var 声明周围换行
    'one-var-declaration-per-line': 0,
    // 警告 对于变量和函数名统一使用驼峰命名法
    'camelcase': 1
  }
}
