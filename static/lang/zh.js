export default {
  nav: {
    home: '首页',
    address: '目的地',
    meitu: '美途',
    cart: '商城',
    my: '我的'
  },
  tab: {
    recommend: '推荐',
    scenic: '景区',
    hotel: '酒店',
    specialty: '特产',
    food: '美食',
    bus: '汽车票',
    train: '火车票',
    airplane: '飞机票',
    strategy: '攻略',
    travels: '游记',
    dynamic: '动态',
    travelsAndDynamic: '游记攻略',
    scenicOrHotel: '酒店/景区',
    traveling: '正在旅行',
    native: '本地人',
    follow: '圈子',
    myOrder: '我的订单',
    waitPay: '待付款',
    cancel: '已取消',
    waitReceive: '待收货',
    quitServer: '退款/售后',
    iCareAbout: '旅行的意义',
    myCollection: '我的收藏',
    myBrowse: '我的足迹',
    myTravels: '我的好友',
    myPublish: '我的喜好'
  },
  title: {
    login: '登录',
    register: '注册',
    registerStep1: '注册(验证手机号)',
    registerStep2: '注册(确认密码)',
    bindPhone: '绑定手机号',
    submitPswd: '确认密码',
    resetPswd: '重置密码',
    findPswd: '找回密码',
    findPswdStep1: '找回密码(验证手机号)',
    findPswdStep2: '重置密码',
    aboutUs: '关于我们',
    archives: '个人主页',
    photoWall: '照片墙',
    photoOtherWall: 'Ta的照片墙',
    comment: '评论',
    report: '举报',
    myStrategy: '我的攻略',
    myTravels: '我的游记',
    myBrowse: '我的足迹',
    myCollection: '我的收藏',
    myPraise: '我的喜好',
    myPublish: '我的旅行',
    order: '我的订单',
    editUserInfo: '修改个人信息',
    fans: '粉丝',
    focus: '关注',
    messageList: '消息列表',
    evalution: '评价晒单',
    logistics: '物流详情',
    orderDesc: '订单详情',
    orderQuitServer: '退款/售后',
    orderQuitDesc: '退款详情',
    addReceiver: '添加收货地址',
    manageReceiver: '收货地址管理',
    updateReceiver: '修改收货地址',
    invoice: '发票',
    myCart: '我的购物车',
    goodsList: '商品清单',
    submitOrder: '提交订单',
    traveling: '正在旅行',
    native: '本地人',
    publishDynamic: '发布动态',
    strategy: '游记攻略',
    panoramaSet: '全景秀',
    videoSet: '视频汇',
    content: '内容详情页',
    findMap: '偶遇-{name}',
    playQdn: '玩转黔东南',
    myDrafts: '我的游记攻略{num}',
    strategyList: '看游记攻略',
    travelsPerfect: '游记信息完善',
    recommend: '精选推荐',
    food: '美食',
    foodDetail: '美食概况',
    storeDetail: '店家概况',
    hotel: '酒店',
    scenic: '景区',
    specialty: '特产',
    specialtyDetail: '特产概况'
  },
  placeholder: {
    phone: '手机号',
    pswd: '密码',
    correctPswd: '请输入6~16位密码',
    againPswd: '再次输入上面的密码',
    code: '验证码',
    playQdn: '玩转黔东南'
  },
  btn: {
    phoneLogin: '手机号登录',
    login: '登 录',
    register: '注册',
    logout: '退出登录',
    forgetPswd: '忘记密码',
    toRegister: '请注册',
    notVip: '不是会员？',
    next: '下一步',
    submit: '提交',
    getCode: '获取验证码',
    refreshCode: '重新获取',
    againSendCode: '重新发送{count}',
    resetPswd: '重置密码',
    thirdLogin: '其他账号登录',
    wechatLogin: '微信',
    qqLogin: 'QQ',
    weiboLogin: '微博',
    refresh: '点击刷新',
    setting: '设置',
    fans: '粉丝',
    focus: '关注',
    message: '消息',
    cart: '购物车',
    onlineService: '智能客服',
    contactUs: '联系我们',
    aboutUs: '关于我们'
  },
  toast: {
    400: '客户端请求的语法错误',
    401: '未授权，请重新登录',
    403: '拒绝访问',
    404: '您所请求的资源无法找到',
    408: '客户端发送的请求时间超时',
    500: '服务器错误',
    501: '服务未实现',
    502: '网络错误',
    503: '服务不可用',
    504: '网络超时',
    505: 'HTTP版本不受支持',
    conFail: '连接服务器失败，错误码：{errCode}',
    conError: '网络请求错误! 详细信息: {errMsg}',
    loading: '加载中...',
    sendCodeSuccess: '发送验证码成功',
    phoneNotCorrect: '手机号码格式不正确',
    codeNotCorrect: '验证码格式不正确',
    emailNotCorrect: '邮箱格式不正确',
    infoNotCorrect: '请完善当前信息',
    twoPswdDiffer: '两次输入的密码不一致',
    pswdRule: '密码长度为6到16位并且只包含数字字母下划线',
    resetPswdSuccess: '重置密码成功',
    loginSuccess: '登录成功！',
    registerSuccess: '注册成功',
    notFindPage: '没有找到对应的页面~',
    loadingMore: '加载更多...',
    notMoreData: '没有更多的数据了',
    refreshedData: '已更新数据',
    mustLogin: '请先登录',
    deleteSuccess: '删除成功',
    nicknameNuoNull: '昵称不能为空',
    phoneNotNull: '手机号码不能为空',
    pswdNotNull: '密码不能为空',
    codeNotNull: '验证码不能为空',
    uploadBgSuccess: '背景图上传成功！',
    updateUserInfoSuccess: '修改个人信息成功！',
    leaveHotelTimeNotNull: '请选择离店时间',
    notContent: '没有详情数据！',
    notData: '喔噢~，没有数据',
    searchNotNull: '请输入关键字搜索~',
    loadMapFail: '网络异常，加载地图组件失败',
    shareSuccess: '分享成功',
    shareCancel: '取消分享',
    shareFail: '分享失败',
    wechatLoginFail: '取消微信登录或者登录失败',
    qqLoginFail: '取消QQ登录或者登录失败',
    weiboLoginFail: '取消微博登录或者登录失败',
    toggleVRSuccess: '切换VR是否显示成功',
    positionFail: '定位失败'
  }
}
