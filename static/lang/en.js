export default {
  nav: {
    home: 'Home',
    address: 'Address',
    meitu: 'Meitu',
    cart: 'Mall',
    my: 'My'
  },
  tab: {
    recommend: 'Recommend',
    scenic: 'Scenic',
    hotel: 'Hotel',
    specialty: 'Specialty',
    food: 'Food',
    strategy: 'Ftrategy',
    travels: 'Travels',
    dynamic: 'News',
    scenicOrHotel: 'Scenic/Hotel',
    traveling: 'Traveling',
    native: 'Native',
    follow: 'Follow',
    myOrder: 'My Orders',
    waitPay: 'Unpaid',
    cancel: 'Cancelled',
    waitReceive: 'Be received',
    quitServer: 'After sale',
    iCareAbout: 'I care about',
    myCollection: 'Collection',
    myBrowse: 'Tracks',
    myTravels: 'friends',
    myPublish: 'Preference'
  },
  title: {
    login: 'Login',
    register: 'Register',
    registerStep1: 'Register(Verify phone)',
    registerStep2: 'Register(Confirm password)',
    bindPhone: 'Bind Phone',
    submitPswd: 'Submit password',
    resetPswd: 'Reset password',
    findPswd: 'Retrieve the password',
    findPswdStep1: 'Retrieve the password(Verify phone)',
    findPswdStep2: 'Retrieve the password(Reset password)',
    aboutUs: 'About Us',
    archives: 'Personal home page',
    photoWall: 'Photo walls',
    comment: 'Comment',
    report: 'Report',
    myStrategy: 'My strategy',
    myTravels: 'My Travels',
    myBrowse: 'My Tracks',
    myCollection: 'My collection',
    myPraise: 'My preference',
    myPublish: 'My trip',
    order: 'My Orders',
    editUserInfo: 'Modifying personal information'
  },
  placeholder: {
    phone: 'Please enter the phone number',
    pswd: 'Please input a password',
    correctPswd: 'Please enter the 6~16 bit password',
    againPswd: 'Enter the above cipher again',
    code: 'Please enter the verification code',
    playQdn: 'Play Qdn'
  },
  btn: {
    login: 'Login',
    register: 'Register',
    logout: 'Logout',
    forgetPswd: 'Forget the password?',
    toRegister: 'Register',
    notVip: 'Not a member?',
    next: 'Next step',
    submit: 'Submit',
    getCode: 'Get code',
    refreshCode: 'Refresh',
    againSendCode: 'Resend{count}',
    resetPswd: 'Reset Password',
    thirdLogin: 'Third Login',
    wechatLogin: 'Wechat Login',
    qqLogin: 'QQ Login',
    weiboLogin: 'Weibo Login',
    refresh: 'Click refresh',
    setting: 'Setting',
    fans: 'Fans',
    focus: 'Follows',
    message: 'Message List',
    cart: 'Cart',
    onlineService: 'Online Service',
    contactUs: 'Contact Us',
    aboutUs: 'About Us'
  },
  toast: {
    400: 'Bad Request',
    401: 'Unauthorized',
    403: 'Forbidden',
    404: 'Not Found',
    408: 'Request Time-out',
    500: 'Internal Server Error',
    501: 'Not Implemented',
    502: 'Bad Gateway',
    503: 'Service Unavailable',
    504: 'Gateway Time-out',
    505: 'HTTP Version not supported',
    conFail: 'Connection server failure, error code: {errCode}',
    conError: 'Network request error! detailed information: {errMsg}',
    loading: 'loading...',
    sendCodeSuccess: 'Send verification code success',
    phoneNotCorrect: 'the phone is not correct',
    codeNotCorrect: 'the varify code is not correct',
    emailNotCorrect: 'the email is not correct',
    infoNotCorrect: 'this infomation is not correct',
    twoPswdDiffer: 'Two inputted password inconsistencies',
    pswdRule: 'The password length is 6 to 16 bits and contains only the numeric alphabet underline.',
    resetPswdSuccess: 'Reset the password success',
    loginSuccess: 'Login Success!',
    registerSuccess: 'Register Success',
    notFindPage: 'No corresponding page was found~',
    notMoreData: 'There is no more data',
    loadingMore: 'loading more...',
    refreshedData: 'Update data completion',
    mustLogin: 'Must Login',
    deleteSuccess: 'Delete Success',
    nicknameNuoNull: 'The nickname cannot be empty',
    phoneNotNull: 'The phone cannot be empty',
    pswdNotNull: 'The password cannot be empty',
    codeNotNull: 'The varify cannot be empty',
    uploadBgSuccess: 'The background map is uploaded successfully!',
    updateUserInfoSuccess: 'Modify personal information successfully!',
    notContent: 'No details!',
    notData: 'Oh oh, no data',
    searchNotNull: 'Please enter the keyword search~',
    loadMapFail: 'Network exception, resulting in the failure of the load map component',
    shareSuccess: 'Sharing success',
    shareCancel: 'Cancellation of sharing',
    shareFail: 'Sharing failure',
    wechatLoginFail: 'Cancellation of WeChat login or login failure',
    qqLoginFail: 'Cancellation of QQ login or login failure',
    weiboLoginFail: 'Cancellation of Weeibo login or login failure',
    positionFail: 'Geographical location failure'
  }
}
