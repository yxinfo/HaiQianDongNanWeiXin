import Vue from 'vue'
import router from './router'
import store from './store'
import moment from 'moment'
import { ToastPlugin, LoadingPlugin, ConfirmPlugin } from 'vux'
import VueLazyLoad from 'vue-lazyload'
import 'vue2-animate/dist/vue2-animate.min.css'
import 'font-awesome/css/font-awesome.css'
import '@/assets/icon/iconfont.css'
import '@/assets/css/other.css'
import VueTouch from 'vue-touch'
import App from './App'
import VueI18n from 'vue-i18n'
import en from '../static/lang/en'
import zh from '../static/lang/zh'

import nativeBridge from '@/assets/js/nativeBridge'
import noImg from '@/assets/img/no_image.jpg'
import loadingImg from '@/assets/img/loading_img.jpg'

// import axios from '@/api/axiosApiNew'

import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/dist/css/swiper.css'
// import { localUser } from '@/assets/js/local'

Vue.use(VueAwesomeSwiper)

Vue.use(VueLazyLoad, {
  error: noImg,
  loading: loadingImg
})

Vue.use(LoadingPlugin)
Vue.use(ToastPlugin)
Vue.use(ConfirmPlugin)
Vue.use(VueTouch, { name: 'v-touch' })

moment.locale('zh-cn')

const FastClick = require('fastclick')
FastClick.attach(document.body)

Vue.prototype.moment = moment
Vue.config.productionTip = false

Vue.use(VueI18n)

// 第三方微信登录
// router.beforeEach((to, from, next) => {
//   if (to.query.requestWechat == null && from.query.requestWechat) {
//     to.query.requestWechat = from.query.requestWechat
//   }
//   if (to.query.requestWechat == null && localUser.get('id') == null) {
//     // 通过在URL地址中添加requestWechat来判断是否已经发送过请求
//     let fullPath = to.fullPath
//     fullPath += (fullPath.indexOf('?') > -1 ? 'requestWechat=1' : '?requestWechat=1')
//     window.location.href = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx3d213297fe8dd31c&redirect_uri=' + encodeURIComponent('http://login.tianlinyong.cn/#' + fullPath) + '&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect'
//     return
//   }
//   // URL解析 微回调地址中code存在 ?....# 中
//   if (document.URL.match(/\?.*#/)) {
//     let temp = document.URL.match(/\?.*#/)[0]
//     if (temp.match(/=.*&/)) {
//       // 解析Code
//       let code = temp.match(/=.*&/)[0]
//       code = code.substr(1, code.length) // 去掉 ?
//       code = code.substr(0, code.length - 1)  // 去掉 #
//       // 通过Code请求获取openId
//       axios.get('/v1/WechatPublicAccount', {
//         code: code
//       }).then((data1) => {
//         // 得到openid
//         localUser.setItem('wpaOpenid', data1.openid)
//         axios.post('/v1/threeWayLogin', {
//           wpaOpenid: data1.openid
//         }).then((data) => {
//           // 通过openid获得用户相关信息
//           alert(JSON.stringify(data))
//           localUser.set(data)
//         }).catch((e) => {
//           next('/login/bindPhone?requestWechat=1')
//         })
//       }).catch(() => {
//         // TODO 是否需要重新发送微信授权请求
//         alert('微信绑定失败~')
//       })
//       // 重置URL，去除code参数
//       let tmpURL = document.URL.replace(temp.substr(0, temp.length - 1), '')
//       window.history.replaceState({}, 0, tmpURL)
//     }
//   }
//   next()
// })

const lang = nativeBridge.getLang()
const i18n = new VueI18n({
  locale: lang,
  messages: {
    zh,
    en
  }
})
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  i18n,
  template: '<App/>',
  components: { App }
})
