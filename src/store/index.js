
import Vue from 'vue'
import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger'

import * as actions from './actions'
import user from './modules/user'
import product from './modules/product'
import dynamic from './modules/dynamic'
import chat from './modules/chat'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  actions,
  modules: {
    user,
    product,
    dynamic,
    chat
  },
  // strict: debug,
  plugins: debug ? [createLogger()] : []
})
