/* 对应 ‘目的地’ 页面 */
import * as types from '../mutation-types'

const state = {
  areaList: [],
  currentArea: {id: 'ba062c32fdf611e7ba2d00163e0c27f8', name: '凯里'},
  cartTabIndex: 0,
  selectOrder: {},  // 选择的订单
  currentComment: null,  // 选择的评论
  trainStationList: [], // 火车站缓存数据
  trainQuery: null,  // 火车票搜索条件
  planeStationList: [] // 飞机票缓存站点数据
}

const getters = {
  selectOrder: state => state.selectOrder,
  areaList: state => state.areaList,
  currentArea: state => state.currentArea,
  cartTabIndex: state => state.cartTabIndex,
  currentComment: state => state.currentComment,
  trainStationList: state => state.trainStationList,
  trainQuery: state => state.trainQuery,
  planeStationList: state => state.planeStationList
}

const mutations = {
  [types.SET_CURRENT_AREA](state, currentArea) {
    state.currentArea = currentArea
  },
  [types.SET_CART_TABINDEX](state, cartTabIndex) {
    state.cartTabIndex = cartTabIndex
  },
  [types.SET_AREA_LIST](state, areaList) {
    state.areaList = areaList
  },
  [types.SET_SELECT_ORDER](state, selectOrder) {
    state.selectOrder = selectOrder
  },
  [types.SET_CURRENT_COMMENT](state, currentComment) {
    state.currentComment = currentComment
  },
  [types.SET_CURRENT_COMMENT](state, currentComment) {
    state.currentComment = currentComment
  },
  [types.SET_TRAIN_STATION_LIST](state, trainStationList) {
    state.trainStationList = trainStationList
  },
  [types.SET_TRAIN_QUERY](state, trainQuery) {
    state.trainQuery = trainQuery
  },
  [types.SET_PLANE_STATION_LIST](state, planeStationList) {
    state.planeStationList = planeStationList
  }
}

const actions = {
}

export default {
  state,
  getters,
  actions,
  mutations
}
