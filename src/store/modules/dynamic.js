/* 对应 ‘美途’ 页面 */
import * as types from '../mutation-types'

const state = {
  meiTuTab: 0,
  nativeArea: 'ba062c32fdf611e7ba2d00163e0c27f8', // 当前选中的本地人
  selectPic: [],  // 发布动态时选择的照片，多个来源
  dynamicDesc: {} // 动态详细中部分数据根据ID获取不到，从上一个页面获取
}

const getters = {
  meiTuTab: state => state.meiTuTab,
  nativeArea: state => state.nativeArea,
  selectPic: state => state.selectPic,
  dynamicDesc: state => state.dynamicDesc
}

const mutations = {
  [types.SET_NATIVE_AREA] (state, nativeArea) {
    state.nativeArea = nativeArea
  },
  [types.SET_MEITU_TAB] (state, meiTuTab) {
    state.meiTuTab = meiTuTab
  },
  [types.PUSH_SELECT_PIC] (state, pic) {
    state.selectPic.push(pic)
  },
  [types.SET_SELECT_PIC] (state, selectPic) {
    state.selectPic = selectPic
  },
  [types.SET_DYNAMIC_DESC] (state, dynamicDesc) {
    state.dynamicDesc = dynamicDesc
  }
}

const actions = {}

export default {
  state,
  getters,
  actions,
  mutations
}
