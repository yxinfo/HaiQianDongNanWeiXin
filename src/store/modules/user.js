
/* 对应 ‘我的’ 页面 */
import * as types from '../mutation-types'

const state = {
  loginAnmation: 'myRotate', // 登录页面的进入退出动画
  scrollTop: {},  // 上一个页面滚动位置的缓存
  orderTab: 0,
  position: [],
  archivesTab: 0,
  collectionTab: 0,
  browseTab: 0,
  praiseTab: 0,
  shoppingList: [],
  invoice: {
    invoiceType: 0,
    invoiceRiseType: 0,
    invoiceContentType: 0,
    companyName: '',
    registrationNumber: '',
    accountPhone: '',
    accountEmail: ''
  }
}

const getters = {
  loginAnmation: state => state.loginAnmation,
  scrollTop: state => state.scrollTop,
  orderTab: state => state.orderTab,
  position: state => state.position,
  archivesTab: state => state.archivesTab,
  collectionTab: state => state.collectionTab,
  browseTab: state => state.browseTab,
  praiseTab: state => state.praiseTab,
  shoppingList: state => state.shoppingList,
  invoice: state => state.invoice
}

const mutations = {
  [types.SET_LOGIN_ANMATION](state, loginAnmation) {
    state.loginAnmation = loginAnmation
  },
  [types.SET_SCROLL_TOP](state, scrollTop) {
    Object.assign(state.scrollTop, scrollTop)
  },
  [types.SET_ORDER_TAB](state, orderTab) {
    state.orderTab = orderTab
  },
  [types.SET_POSITION](state, position) {
    state.position = position
  },
  [types.SET_ARCHIVES_TAB](state, archivesTab) {
    state.archivesTab = archivesTab
  },
  [types.SET_COLLECTION_TAB](state, collectionTab) {
    state.collectionTab = collectionTab
  },
  [types.SET_BROWSE_TAB](state, browseTab) {
    state.browseTab = browseTab
  },
  [types.SET_PRAISE_TAB](state, praiseTab) {
    state.praiseTab = praiseTab
  },
  [types.SET_SHOPPING_LIST](state, shoppingList) {
    state.shoppingList = shoppingList
  },
  [types.SET_INVOICE](state, invoice) {
    state.invoice = invoice
  }
}

const actions = {}

export default {
  state,
  getters,
  actions,
  mutations
}
