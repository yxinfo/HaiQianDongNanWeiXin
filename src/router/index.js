import Vue from 'vue'
import Router from 'vue-router'

// 五个Tab页全部预加载
import Address from '@/pages/address/address'
import MeiTu from '@/pages/meiTu/meiTu'
import Cart from '@/pages/cart/cart'
import User from '@/pages/User/User'
Vue.use(Router)

// 其他的页面懒加载

// 首页
const City = () => import('@/pages/home/city')

// 目的地
const PlayStrategy = () => import('@/pages/address/playStrategy/playStrategy')
const PlayStrategyList = () => import('@/pages/address/playStrategy/playStrategyList')

const AddressMap = () => import('@/pages/address/map/map')
const Drafts = () => import('@/pages/address/playStrategy/drafts')
const TravelsEdit = () => import('@/pages/address/publishTravels/travelsEdit')
const TravelsPerfect = () => import('@/pages/address/publishTravels/travelsPerfect')
const Raiders = () => import('@/pages/address/raiders/raiders')

const Strategy = () => import('@/pages/address/raiders/strategy')
const StrategyDetail = () => import('@/pages/address/raiders/strategyDetail')

// 游记
const Travels = () => import('@/pages/address/playStrategy/travels')

// 美途
const DynamicDesc = () => import('@/pages/meiTu/dynamicDesc/dynamicDesc')

// 商城
const Food = () => import('@/pages/cart/cart-page/food/food')
const FoodDescribe = () => import('@/pages/cart/cart-page/food/foodDescribe')
const StoreDescribe = () => import('@/pages/cart/cart-page/food/storeDescribe')
const FoodList = () => import('@/pages/cart/cart-page/food/foodDetails/foodList')
const FoodDetailsOne = () => import('@/pages/cart/cart-page/food/foodDetails/foodDetailsOne')

// 商城-景区
const ScenicList = () => import('@/pages/cart/cart-page/scenic/scenicList')
const Scenic = () => import('@/pages/cart/cart-page/scenic/scenic')
const ScenicVideoSet = () => import('@/pages/cart/cart-page/scenic/scenicVideoSet')
const ScenicOrder = () => import('@/pages/cart/cart-page/scenic/scenicOrder')
const ScenicOrderDetail = () => import('@/pages/cart/cart-page/scenic/scenicOrderDetail')
const ServiceMap = () => import('@/pages/cart/serviceMap')

// 我的
const Login = () => import('@/pages/user/login/login')
const BindPhone = () => import('@/pages/user/login/bindPhone')
const About = () => import('@/pages/user/about/about')
const Privacy = () => import('@/pages/user/about/privacy')
const ServerCase = () => import('@/pages/user/about/server')
const Collection = () => import('@/pages/user/collection/collection')
const Browse = () => import('@/pages/user/browse/browse')
const Fans = () => import('@/pages/user/follower/fans')
const Focus = () => import('@/pages/user/follower/focus')
const MessageList = () => import('@/pages/user/message/messageList')
const Friends = () => import('@/pages/user/publish/friends')
const NewFriends = () => import('@/pages/user/publish/newfriends')
const Praise = () => import('@/pages/user/publish/praise')
const Archives = () => import('@/pages/user/archives/archives')
const TravelMiddle = () => import('@/pages/user/archives/travelmiddle')
const StrategyMiddle = () => import('@/pages/user/archives/strategymiddle')
const PhotoWall = () => import('@/pages/user/archives/photowall')

const Chat = () => import('@/components/chat/chat')
const ChatAI = () => import('@/components/chat/chatAI')

const CommontContent = () => import('@/pages/commonContent/content')

const CommonReport = () => import('@/pages/report/report')

const HotelPortal = () => import('@/pages/cart/cart-page/hotel/hotelPortal')
const HotelList = () => import('@/pages/cart/cart-page/hotel/hotelList')
const Hotel = () => import('@/pages/cart/cart-page/hotel/hotel')
const HotelOrder = () => import('@/pages/cart/cart-page/hotel/hotelOrder')
const HotelOrderDetail = () => import('@/pages/cart/cart-page/hotel/hotelOrderDetail')

const RouterInstance = new Router({
  // mode: 'history',
  routes: [
    {
      path: '/',
      redirect: '/city'
    },
    {
      path: '/address',
      name: 'Address',
      component: Address,
      children: [
        {
          path: 'map/:addressId/:type',
          name: 'AddressMap',
          component: AddressMap
        },
        {
          path: 'playStrategy/:playStrategyId',
          name: 'PlayStrategy',
          component: PlayStrategy
        },
        {
          path: 'playStrategyList',
          name: 'PlayStrategyList',
          component: PlayStrategyList
        },
        {
          path: 'drafts',
          name: 'Drafts',
          component: Drafts
        },
        {
          path: 'travelsEdit/:type',
          name: 'TravelsEdit',
          component: TravelsEdit
        },
        {
          path: 'travelsPerfect/:id',
          name: 'TravelsPerfect',
          component: TravelsPerfect
        },
        {
          path: 'strategy/:id',
          name: 'Strategy',
          component: Strategy
        },
        {
          path: 'strategyDetail/:id',
          name: 'StrategyDetail',
          component: StrategyDetail
        },
        {
          path: 'travels/:travelsId',
          name: 'Travels',
          component: Travels
        }
      ]
    },
    {
      path: '/meiTu',
      name: 'MeiTu',
      component: MeiTu,
      children: [
        {
          path: 'dynamicDesc/:dynamicId',
          name: 'DynamicDesc',
          component: DynamicDesc
        }
      ]
    },
    {
      path: '/cart',
      name: 'Cart',
      component: Cart,
      children: [
        {
          path: '/foodList/:addressInfoId',
          name: 'FoodList',
          component: FoodList
        },
        {
          path: '/foodDetailsOne/:id',
          name: 'FoodDetailsOne',
          component: FoodDetailsOne
        }
      ]
    },
    {
      path: '/user',
      name: 'User',
      component: User,
      children: [
        {
          path: 'about',
          name: 'About',
          component: About
        },
        {
          path: 'collection',
          name: 'Collection',
          component: Collection
        },
        {
          path: 'browse',
          name: 'Browse',
          component: Browse
        },
        {
          path: 'fans/:userId',
          name: 'Fans',
          component: Fans
        },
        {
          path: 'focus/:userId',
          name: 'Focus',
          component: Focus
        },
        {
          path: 'messageList',
          name: 'MessageList',
          component: MessageList
        },
        {
          path: 'friends',
          name: 'Friends',
          component: Friends
        },
        {
          path: 'newfriends',
          name: 'NewFriends',
          component: NewFriends
        },
        {
          path: 'praise',
          name: 'Praise',
          component: Praise
        },
        {
          path: 'archives/:userId',
          name: 'Archives',
          component: Archives
        },
        {
          path: 'travelmiddle',
          name: 'TravelMiddle',
          component: TravelMiddle
        },
        {
          path: 'strategymiddle',
          name: 'StrategyMiddle',
          component: StrategyMiddle
        },
        {
          path: 'photowall/:userId',
          name: 'PhotoWall',
          component: PhotoWall
        }
      ]
    },
    {
      path: '/chat',
      name: 'Chat',
      component: Chat
    },
    {
      path: '/chatAI',
      name: 'ChatAI',
      component: ChatAI
    },
    {
      path: '/foodDescribe/:id',
      name: 'FoodDescribe',
      component: FoodDescribe
    },
    {
      path: '/storeDescribe/:contentId',
      name: 'StoreDescribe',
      component: StoreDescribe
    },
    {
      path: '/content/:contentId/:title',
      name: 'CommontContent',
      component: CommontContent
    },
    {
      path: '/commonReport/:targetType/:targetId',
      name: 'CommonReport',
      component: CommonReport
    },
    {
      path: '/food',
      name: 'Food',
      component: Food
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
      children: [
        {
          path: 'bindPhone',
          name: 'BindPhone',
          component: BindPhone
        }
      ]
    },
    {
      path: '/privacy',
      name: 'Privacy',
      component: Privacy
    },
    {
      path: '/servercase',
      name: 'ServerCase',
      component: ServerCase
    },
    {
      path: '/city',
      name: City,
      component: City
    },
    {
      path: '/raiders',
      component: Raiders,
      name: 'Raiders'
    },
    {
      path: '/hotelPortal',
      name: 'HotelPortal',
      component: HotelPortal
    },
    {
      path: '/hotelList',
      name: 'HotelList',
      component: HotelList
    },
    {
      path: '/hotel/:id',
      name: 'Hotel',
      component: Hotel
    },
    {
      path: '/hotelOrder',
      name: 'HotelOrder',
      component: HotelOrder
    },
    {
      path: '/hotelOrderDetail/:tradeNo',
      name: 'HotelOrderDetail',
      component: HotelOrderDetail
    },
// 景区部分
    {
      path: '/scenicList',
      name: 'ScenicList',
      component: ScenicList
    },
    {
      path: '/scenic/:id',
      name: 'Scenic',
      component: Scenic
    },
    {
      path: '/scenicVideoSet/:id',
      name: 'ScenicVideoSet',
      component: ScenicVideoSet
    },
    {
      path: '/scenicOrder',
      name: 'ScenicOrder',
      component: ScenicOrder
    },
    {
      path: '/scenicOrderDetail/:tradeNo',
      name: 'ScenicOrderDetail',
      component: ScenicOrderDetail
    },
    {
      path: '/serviceMap/:id/:type',
      name: 'ServiceMap',
      component: ServiceMap
    }
  ]
})

export default RouterInstance
