import { isIOS, isAndroid } from './brower'

export default {
  shareToWechat: function (vueInstance, type, title, desc, thumbUrl, linkUrl) {
    window._Native_wechatShare = (arg) => {
      /* eslint-disable */
      if (arg == 0) {
        vueInstance.$vux.toast.text(vueInstance.$t('toast.shareSuccess'), 'middle')
      } else if (arg == -2) {
        vueInstance.$vux.toast.text(vueInstance.$t('toast.shareCancel'), 'middle')
      } else {
        vueInstance.$vux.toast.text(vueInstance.$t('toast.shareFail'), 'middle')
      }
      /* eslint-enable */
    }
    if (isIOS()) {
      if (type === 0) {
        type = 'WXSceneSession'
      } else {
        type = 'WXSceneTimeline'
      }
      window.webkit.messageHandlers.NativeMethod.postMessage({
        'methodName': 'wechatShare',
        'shareType': type,
        'title': title,
        'desc': desc,
        'img': thumbUrl,
        'webpage': linkUrl
      })
    } else {
      if (window.android) {
        window.android.WxUrlShare(linkUrl, title, desc, thumbUrl, type)
      }
    }
  },
  shareToQQ: function (vueInstance, type, title, desc, thumbUrl, linkUrl) {
    // type 0 好友  1 空间
    if (isIOS()) {
      window._Native_qqShare = (result) => {
        /* eslint-disable */
        if (result == 0) {
          vueInstance.$vux.toast.text(vueInstance.$t('toast.shareSuccess'), 'middle')
        } else if (result == -4) {
          vueInstance.$vux.toast.text(vueInstance.$t('toast.shareCancel'), 'middle')
        } else {
          vueInstance.$vux.toast.text(vueInstance.$t('toast.shareFail'), 'middle')
        }
        /* eslint-enable */
      }
      if (type === 0) {
        type = 'QFriend'
      } else {
        type = 'QZone'
      }
      window.webkit.messageHandlers.NativeMethod.postMessage({
        'methodName': 'qqShare',
        'shareType': type,
        'title': title,
        'desc': desc,
        'img': thumbUrl,
        'webpageUrl': linkUrl
      })
    } else {
      window._Native_qqShare = (result) => {
        if (result === 'success') {
          vueInstance.$vux.toast.text(vueInstance.$t('toast.shareSuccess'), 'middle')
        } else if (result === 'cancel') {
          vueInstance.$vux.toast.text(vueInstance.$t('toast.shareCancel'), 'middle')
        } else {
          vueInstance.$vux.toast.text(vueInstance.$t('toast.shareFail'), 'middle')
        }
      }
      if (window.android) {
        window.android.qqShare(title, desc, linkUrl, thumbUrl, type)
      }
    }
  },
  shareToWeibo: function (vueInstance, title, desc, thumbUrl, linkUrl) {
    window._Native_weiboShare = (result) => {
      if (result === 'success') {
        vueInstance.$vux.toast.text(vueInstance.$t('toast.shareSuccess'), 'middle')
      } else if (result === 'cancel') {
        vueInstance.$vux.toast.text(vueInstance.$t('toast.shareCancel'), 'middle')
      } else {
        vueInstance.$vux.toast.text(vueInstance.$t('toast.shareFail'), 'middle')
      }
    }
    if (isIOS()) {
      window.webkit.messageHandlers.NativeMethod.postMessage({
        'methodName': 'shareToWeibo',
        'title': title,
        'desc': desc,
        'img': thumbUrl,
        'webpageUrl': linkUrl
      })
    } else {
      if (window.android) {
        window.android.wbShare(title, desc, linkUrl, thumbUrl)
      }
    }
  },
  wechatLogin: function (vueInstance, callback) {
    if (isIOS()) {
      window._Native_wechatLogin = (arg) => {
        if (arg.errcode) {
          vueInstance.$vux.toast.text(vueInstance.$t('toast.wechatLoginFail'), 'middle')
        } else {
          callback(arg)
        }
      }
      window.webkit.messageHandlers.NativeMethod.postMessage({
        'methodName': 'wechatLogin'
      })
    } else {
      window._Native_wechatLogin = (arg) => {
        if (arg.errCode) {
          vueInstance.$vux.toast.text(vueInstance.$t('toast.wechatLoginFail'), 'middle')
        } else {
          callback(arg)
        }
      }
      window.android.wechatLogin()
    }
  },
  qqLogin: function (vueInstance, callback) {
    if (isIOS()) {
      window._Native_qqLogin = (arg) => {
        if (arg.ret !== 0) {
          vueInstance.$vux.toast.text(vueInstance.$t('toast.qqLoginFail'), 'middle')
        } else {
          callback(arg)
        }
      }
      window.webkit.messageHandlers.NativeMethod.postMessage({ 'methodName': 'qqLogin' })
    } else {
      // window._Native_qqLogin = (arg) => {
      //   if (arg.errcode) {
      //     vueInstance.$vux.toast.text('取消QQ登录或者登录失败', 'middle')
      //   } else {
      //     callback(arg)
      //   }
      // }
      // window.android.qqLogin()
      vueInstance.$vux.toast.text('软件著作权审核中，暂不开放', 'middle')
    }
  },
  weiboLogin: function (vueInstance, callback) {
    if (isIOS()) {
      window._Native_weiboLogin = (result) => {
        callback(result)
      }
      window._Native_weiboCancelLogin = () => {
        vueInstance.$vux.toast.text(vueInstance.$t('toast.weiboLoginFail'), 'middle')
      }
      window.webkit.messageHandlers.NativeMethod.postMessage({ 'methodName': 'weiboLogin' })
    } else {
      window._Native_weiboLogin = (arg) => {
        callback(arg)
      }
      window.android.wbLogin()
    }
  },
  alipay: function (outTradeNo, callback) {
    window._Native_aliPay = (arg) => {
      let msg = ''
      switch (arg) {
        case 9000:
          msg = '订单支付成功'
          break
        case 8000:
          msg = '正在处理中，支付结果未知'
          break
        case 4000:
          msg = '订单支付失败'
          break
        case 5000:
          msg = '重复请求'
          break
        case 6001:
          msg = '用户中途取消'
          break
        case 6002:
          msg = '网络连接出错'
          break
        case 6004:
          msg = '支付结果未知，请查询订单状态'
          break
        default:
          msg = '其他支付错误'
          break
      }
      /* eslint-disable */
      callback({arg, msg})
      /* eslint-enable */
    }
    if (isIOS()) {
      window.webkit.messageHandlers.NativeMethod.postMessage({
        'methodName': 'alipay',
        'outTradeNo': outTradeNo
      })
    } else {
      if (window.android) {
        window.android.aliPay(outTradeNo)
      }
    }
  },
  getVRStatus: function (callback) {
    /* eslint-disable */
    if (isIOS()) {
      window._Native_getVRStatus = (arg) => {
        callback(!arg)
      }
      window.webkit.messageHandlers.NativeMethod.postMessage({
        'methodName': 'hasSkipVr'
      })
    } else {
      if (window.android) {
        callback(window.android.isStartVr())
      }
    }
    /* eslint-enable */
  },
  toggleVR: function (vueInstance, status) {
    if (isIOS()) {
      window._Native_toggleVR = (arg) => {
        vueInstance.$vux.toast.text('设置成功', 'middle')
      }
      window.webkit.messageHandlers.NativeMethod.postMessage({
        'methodName': 'skipVr',
        'skip': status ? 'NO' : 'YES'
      })
    } else {
      if (window.android) {
        window.android.switchOpen(status)
        vueInstance.$vux.toast.text('设置成功', 'middle')
      }
    }
  },
  setJPushAlias: function (userId) {
    if (isIOS()) {
      if (window.webkit) {
        window.webkit.messageHandlers.NativeMethod.postMessage({
          'methodName': 'setAlias',
          'userId': userId
        })
      }
    } else {
      if (window.android) {
        window.android.setAliasApi(userId)
      }
    }
  },
  getPosition: function (vueInstance, callback) {
    if (isIOS()) {
      window._Native_location = (position, geo, status) => {
        if (status !== 0 && status !== '0') {
          vueInstance.$vux.toast.text('定位失败', 'middle')
          return
        }
        callback(position, geo)
      }
      window.webkit.messageHandlers.NativeMethod.postMessage({
        'methodName': 'location'
      })
    } else {
      window._Native_location = (arg) => {
        const tmp = JSON.parse(arg)
        // const tmpgeo = JSON.parse(geo)
        if (tmp.errorCode !== 0 && status !== '0') {
          vueInstance.$vux.toast.text('定位失败', 'middle')
          return
        }
        /* eslint-disable */
        callback({
          longitude: tmp.longitude,
          latitude: tmp.latitude
        }, tmp)
        /* eslint-enable */
      }
      if (window.android) {
        window.android.getPositioning()
      }
    }
  },
  // 语音读取
  speechVoice: function (str, success) {
    window._Native_speechComplete = success
    if (isIOS()) {
      window.webkit.messageHandlers.NativeMethod.postMessage({
        'methodName': 'iflySpeech',
        'speechContent': str
      })
    } else {
      window.android.speechSynthesizerVoice(str)
    }
  },
  // 停止语音播放
  stopSpeechVoice: function () {
    if (isIOS()) {
      window.webkit.messageHandlers.NativeMethod.postMessage({
        'methodName': 'stopSpeechSynthesizerVoice'
      })
    } else {
      window.android.stopSpeechSynthesizerVoice()
    }
  },
  // 停止语音播放
  pauseSpeechVoice: function () {
    if (isIOS()) {
      window.webkit.messageHandlers.NativeMethod.postMessage({
        'methodName': 'pauseSpeechSynthesizerVoice'
      })
    } else {
      window.android.pauseSpeechSynthesizerVoice()
    }
  },
  // 语音识别
  recognizerVoice: function (resultCallback, volumeListener) {
    window._Native_recognizerResult = resultCallback
    window._Native_recognizerVolume = volumeListener
    if (isIOS()) {
      window.webkit.messageHandlers.NativeMethod.postMessage({
        'methodName': 'startIflyRecognizer'
      })
    } else {
      window.android.speechRecognizerVoice()
    }
  },
  // 取消语音识别
  cancelRecognizerVoice: function () {
    if (isIOS()) {
      window.webkit.messageHandlers.NativeMethod.postMessage({
        'methodName': 'cancelIflyRecognizer'
      })
    } else {
      window.android.cancelSpeechRecognizerVoice()
    }
  },
  // 结束语音识别
  stopRecognizerVoice: function () {
    if (isIOS()) {
      window.webkit.messageHandlers.NativeMethod.postMessage({
        'methodName': 'stopIflyRecognizer'
      })
    } else {
      window.android.stopSpeechRecognizerVoice()
    }
  },
  /**
   * Android返回按钮的拦截
   * @param {Function} callback Android返回按钮的拦截 true 拦截   false 不拦截
   */
  handleNativeBack: function (callback) {
    if (isAndroid()) {
      window._Native_backListener = () => {
        if (callback) {
          const result = callback()
          return result
        }
        return false
      }
    }
  },
  /**
   * IOS的侧滑返回的禁用和开放
   * @param {Boolean} allow IOS的侧滑返回 true 禁用   false 开放
   */
  toggleIOSBack: function (allow) {
    if (isIOS()) {
      window.webkit.messageHandlers.NativeMethod.postMessage({
        'methodName': 'switchBackWithGestures',
        'allow': allow ? 'false' : 'true'
      })
    }
  },
  /**
   * 获取IOS版本号
   */
  appVersion: function (callback) {
    if (isIOS()) {
      window._Native_appVersion = (code) => {
        callback(code)
      }
      window.webkit.messageHandlers.NativeMethod.postMessage({
        'methodName': 'appVersion'
      })
    } else {
      const code = window.android.getVersionCode()
      callback(code)
    }
  },
  /**
   * 获取手机设置的语言
   */
  getLang: function () {
    // APP暂时只支持中文 zh  和  英文 en
    let lang = 'zh'
    if (isIOS()) {

    } else {
      if (window.android) {
        lang = window.android.getLanguage()
        if (lang !== 'en') {
          lang = 'zh'
        }
      }
    }
    return lang
  },
  /**
   * 打开H5页面。
   * iframe存在跨域问题，交给原生处理
   */
  openH5Page: function (pageUrl) {
    if (isIOS()) {
      window.webkit.messageHandlers.NativeMethod.postMessage({
        'methodName': 'openPage',
        'url': pageUrl
      })
    } else {
      window.android.openPage(pageUrl)
    }
  },
  /**
   * 设置状态栏
   * color   设置颜色（white, black）
   */
  setStatusBar: function (color) {
    if (isIOS()) {
      if (window.webkit) {
        window.webkit.messageHandlers.NativeMethod.postMessage({
          'methodName': 'statusBarColor',
          'color': color
        })
      }
    } else {
      if (window.android) {
        window.android.setWStatusBarColor(color)
      }
    }
  }
}
