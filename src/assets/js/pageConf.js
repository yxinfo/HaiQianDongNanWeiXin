/**
 * 未设置默认白色
 * 格式： (routeName: param)
 * statusBarColor: 暂时只支持white和black
 * disableIOSBack: true禁用全局IOS侧滑返回
 * 注意: 1、statusBarColor动态变化的时候，需要引入statusBarMixin。
 *       2、侧滑返回的时候页面弹窗需要特殊处理
 *       3、这样写麻烦一点，方便以后扩展
 */
export default {
  _default: {
    statusBarColor: 'white',
    disableIOSBack: false
  },
  Home: {
    disableIOSBack: true
  },
  Address: {
    disableIOSBack: true
  },
  MeiTu: {
    disableIOSBack: true
  },
  Cart: {
    statusBarColor: 'black',
    disableIOSBack: true
  },
  User: {
    disableIOSBack: true
  },
  PlayStrategy: {
    statusBarColor: 'black'
  },
  FoodDescribe: {
    statusBarColor: 'black'
  },
  StoreDescribe: {
    statusBarColor: 'black'
  },
  FoodList: {
    statusBarColor: 'black'
  }
}
