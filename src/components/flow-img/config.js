export default {
  minHeight: 120,
  maxHeight: 210,
  heightScale: 0.5,
  height: 140,
  maxWidth: window.innerWidth * 0.68
}
