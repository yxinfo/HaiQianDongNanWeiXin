import axios from 'axios'

const URL = 'http://www.tuling123.com/openapi/api'
const key = '5ab30db5cd734e099b7e5687a33377e9'

export default function (info, loc, userid) {
  const params = {
    key,
    info,
    loc,
    userid
  }
  return new Promise((resolve, reject) => {
    axios({
      method: 'POST',
      url: URL,
      params: params,
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      }
    }).then((res) => {
      console.log(res)
      // 暂时只处理文本类的数据，其他的视为错误
      if (res.data.code === 100000) {
        resolve(res.data.text)
      } else {
        reject(new Error('不支持该格式的返回结果'))
      }
    })
  })
}
