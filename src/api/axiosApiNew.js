import axios from 'axios'
import { BASE_URL, OK } from './config'
import qs from 'qs' // qs.parse()将URL解析成对象的形式, qs.stringify()将对象 序列化成URL的形式

// 自定义判断元素类型JS
function toType (obj) {
  return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase()
}
// 参数过滤函数
function filterNull (o) {
  for (var key in o) {
    if (o[key] === null) {
      delete o[key]
    }
    if (toType(o[key]) === 'string') {
      o[key] = o[key].trim()
    } else if (toType(o[key]) === 'object') {
      o[key] = filterNull(o[key])
    } else if (toType(o[key]) === 'array') {
      o[key] = filterNull(o[key])
    }
  }
  return o
}

class CustomError extends Error {
  constructor(code, ...params) {
    super(...params)
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, CustomError)
    }
    this.code = code
    this.date = new Date()
  }
}

/**
 * 通用的Api处理方法
 * 如有特殊情况的请求方式，请另外写
 * @param {String} method GET、PUT、POST、DELETE
 * @param {String} url URL地址，域名等通用字段已配置
 * @param {Object} params 参数对象
 */
function apiAxios (method, url, params) {
  // 过滤为空的参数
  if (params) {
    params = filterNull(params)
  }
  let baseURL = BASE_URL
  if (url.indexOf('http') > 0) {
    baseURL = ''
  }
  return new Promise((resolve, reject) => {
    axios({
      method: method,
      url: url,
      data: method === 'POST' || method === 'PUT' ? qs.stringify(params) : null,
      params: method === 'GET' || method === 'DELETE' ? params : null,
      baseURL: baseURL,
      withCredentials: false
    }).then(function (res) {
      if (res.data.code === OK) {
        if (res.data.total || res.data.total === 0) {
          resolve({data: res.data.data, total: res.data.total})
        } else {
          resolve(res.data.data)
        }
      } else {
        reject(new CustomError(res.data.code, res.data.message))
        console.error('api error: ' + res.data.message)
      }
    }).catch(function (err) {
      // alert(err.message)
      reject(err)
      if (err) {
        console.error('api error, HTTP CODE: ' + err.message)
      }
    })
  })
}

export default {
  /**
   * get方法
   */
  get: function (url, params) {
    return apiAxios('GET', url, params)
  },
  post: function (url, params) {
    return apiAxios('POST', url, params)
  },
  put: function (url, params, success) {
    return apiAxios('PUT', url, params)
  },
  delete: function (url, params) {
    return apiAxios('DELETE', url, params)
  }
}