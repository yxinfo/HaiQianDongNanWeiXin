import axios from 'axios'
import { GDMAP_KEY_WEB } from './config'

const URL = 'http://restapi.amap.com/v3/weather/weatherInfo?parameters'
let params = {
  key: GDMAP_KEY_WEB,
  extensions: 'base',   // base 实况天气   all 预报天气
  output: 'json'
}

export default function (city, success, failure) {
  params.city = city
  axios({
    method: 'GET',
    url: URL,
    params: params
  }).then((res) => {
    if (res.data.status === '1' && res.data.count > 0) {
      if (success) {
        success(res.data.lives[0])
      }
    } else {
      if (failure) {
        failure(res.data.info)
      }
      console.error('该城市天气数据为空')
    }
  }).catch(e => {
    if (failure) {
      failure('获取天气错误')
    }
    console.error('获取天气错误。详细信息：' + e.message)
  })
}
